#!/usr/bin/python3
# coding: utf-8

@profile
def function_to_test():
    huge_list = [0] * (10 ** 6)
    _ = ['abcdefgh'] * (5 ** 10)
    del huge_list


if __name__ == '__main__':
    function_to_test()
