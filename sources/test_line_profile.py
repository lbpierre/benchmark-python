#!/usr/bin/python3
# coding: utf-8

import time

@profile
def fast():
    print("dummy print!")

@profile
def slow():
    time.sleep(5)
    print("after sleep")


if __name__ == '__main__':
    fast()
    slow()
