#!/usr/bin/python3
# coding: utf-8

import numpy as np
from random import randint

@profile
def create_list_with_numpy():
    # return np.array([0] * (10**8))
    return np.arange(10**8)


@profile
def create_built_in_list():
    return [0] * (10**8)


@profile
def random_list_np(array):
    lenght = len(array)

    for i in range(10**6):
        index, value = randint(0, lenght), randint(0, 10**10)
        array[index] = value


@profile
def random_list_built_in(array):
    lenght = len(array)

    for i in range(10**6):
        index, value = randint(0, lenght), randint(0, 10**10)
        array[index] = value


if __name__ == "__main__":
    built_in = create_built_in_list()
    np_array = create_list_with_numpy()

    random_list_built_in(built_in)
    random_list_np(np_array)
