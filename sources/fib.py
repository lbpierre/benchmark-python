#!/usr/bin/python3
# coding: utf-8

def fib(n):
    if n <= 1:
        return n
    else:
        return (fib(n-1) + fib(n-2))


if __name__ == "__main__":
    out = fib(30)
    print(out)
