#!/usr/bin/python3
# coding: utf-8

from numba import njit
import numpy as np
nums = np.array([11, 0, 1, 10, 5, 19], dtype=np.int64)


@njit
def solution(nums, N):
    last = {}
    for i, x in enumerate(nums[:-1]):
        last[x] = i
    buffer = nums[-1]
    for i in range(len(nums) - 1, N - 1):
        x = i - last[buffer] if buffer in last else 0
        last[buffer] = i
        buffer = x
    return buffer


print(solution(nums, 2020))
print(solution(nums, 30000000))
