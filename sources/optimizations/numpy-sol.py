#!/usr/bin/python3
# coding: utf-8

import numpy as np
nums = np.array([5, 1, 9, 18, 13, 8, 0], dtype=np.int64)


@profile
def solution(nums, N):
    last = {}
    for i, x in enumerate(nums[:-1]):
        last[x] = i
    buffer = nums[-1]
    for i in range(len(nums) - 1, N - 1):
        x = i - last[buffer] if buffer in last else 0
        last[buffer] = i
        buffer = x
    return buffer


solution(nums, 2020)
solution(nums, 30000000)
