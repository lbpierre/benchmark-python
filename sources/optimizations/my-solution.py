#!/usr/bin/python3
# coding: utf-8

from typing import List


@profile
def play(inputs: List[int], iteration: int) -> int:

    tape = {v: i for i, v in enumerate(inputs)}
    index = 0

    for i in range(len(inputs), iteration-1):
        tape[index], index = i, i - tape.get(index, i)

    return index


def solve(inputs: List[str]):

    inputs = [int(i) for i in inputs[0].split(',')]
    play(inputs, 2020)
    play(inputs, 30000000)


if __name__ == "__main__":
    solve("5,1,9,18,13,8,0")
