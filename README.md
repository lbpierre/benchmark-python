Benchmark
=========

Tools
-----

## CProfile

> Requirement: None (built-in)

```
python -m cProfile sources/fib.py
832040
2692541 function calls (5 primitive calls) in 0.413 seconds

Ordered by: standard name

ncalls  tottime  percall  cumtime  percall filename:lineno(function)
	1    0.000    0.000    0.413    0.413 fib.py:4(<module>)
	2692537/1    0.413    0.000    0.413    0.413 fib.py:4(fib)
    1    0.000    0.000    0.413    0.413 {built-in method builtins.exec}
    1    0.000    0.000    0.000    0.000 {built-in method builtins.print}
	1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
```

## Line_profile

> Requirement: `pip install line-profiler`

Usage:

```
kernprof -l sources/test_line_profile.py
python -m line_profiler sources/test_line_profile.py.lprof 
```

Output:
```
Timer unit: 1e-06 s

Total time: 1.7e-05 s
File: test_line_profile.py
Function: fast at line 6

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
     6                                           @profile
     7                                           def fast():
     8         1         17.0     17.0    100.0      print("dummy print!")

Total time: 5.0053 s
File: test_line_profile.py
Function: slow at line 10

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    10                                           @profile
    11                                           def slow():
    12         1    5005247.0 5005247.0    100.0      time.sleep(5)
    13         1         50.0     50.0      0.0      print("after sleep")
```

## mem_profile

> Requirements: `pip install memory-profiler`


```
@profile
def function_to_test():
    huge_list = [0] * (10 ** 6)
    _ = ['abcdefgh'] * (5 ** 10)
    del huge_list


if __name__ == '__main__':
    function_to_test()
```

Run it!
```
mprof run memo_prof.py 
mprof plot
```

Output:
```
Filename: test_mem_profile.py
Line   Mem usage    Increment  Occurences   Line Contents
============================================================
     4   37.770 MiB   37.770 MiB           1   @profile
     5                                         def function_to_test():
     6   45.410 MiB    7.641 MiB           1       huge_list = [0] * (10 ** 6)
     7  119.918 MiB   74.508 MiB           1       _ = ['abcdefgh'] * (5 ** 10)
     8  112.305 MiB   -7.613 MiB           1       del huge_list
```

![plot](images/mem_profile.png?raw)


## profilehook

> Requirement: `pip install profilehook`

Output:
```
***PROFILER RESULTS ***
function_to_test (hook.py:7)
function called 1 times

         2 function calls in 0.025 seconds

   Ordered by: cumulative time, internal time, call count

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.025    0.025    0.025    0.025 hook.py:7(function_to_test)
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        0    0.000             0.000          profile:0(profiler)
```


## scalene

> Requirement: `pip install scalene`

![scalene output](images/scalene-built-in.png)


Numpy
=====

```
#!/usr/bin/python3
# coding: utf-8

import numpy as np
from random import randint

@profile
def create_list_with_numpy():
    # return np.array([0] * (10**8))
    return np.arange(10**8)


@profile
def create_built_in_list():
    return [0] * (10**8)


@profile
def random_list_np(array):
    lenght = len(array)

    for i in range(10**6):
        index, value = randint(0, lenght), randint(0, 10**10)
        array[index] = value


@profile
def random_list_built_in(array):
    lenght = len(array)

    for i in range(10**6):
        index, value = randint(0, lenght), randint(0, 10**10)
        array[index] = value


if __name__ == "__main__":
    built_in = create_built_in_list()
    np_array = create_list_with_numpy()

    random_list_built_in(built_in)
    random_list_np(np_array)
```

![scalene benchmark on np-or-list.py](images/scalene_np_built_in.png)


Exercice
========

[Advent Of Code 2020 day 15](https://adventofcode.com/2020/day/15)

> You catch the airport shuttle and try to book a new flight to your vacation island. Due to the storm, all direct flights have been cancelled, but a route is available to get around the storm. You take it.
>
>While you wait for your flight, you decide to check in with the Elves back at the North Pole. They're playing a memory game and are ever so excited to explain the rules!
>
>In this game, the players take turns saying numbers. They begin by taking turns reading from a list of starting numbers (your puzzle input). Then, each turn consists of considering the most recently spoken number:
>
>    If that was the first time the number has been spoken, the current player says 0.
>    Otherwise, the number had been spoken before; the current player announces how many turns apart the number is from when it was previously spoken.
>
>So, after the starting numbers, each turn results in that player speaking aloud either 0 (if the last number is new) or an age (if the last number is a repeat).

>For example, suppose the starting numbers are 0,3,6:
>
>    Turn 1: The 1st number spoken is a starting number, 0.
>    Turn 2: The 2nd number spoken is a starting number, 3.
>    Turn 3: The 3rd number spoken is a starting number, 6.
>    Turn 4: Now, consider the last number spoken, 6. Since that was the first time the number had been spoken, the 4th number spoken is 0.
 >   Turn 5: Next, again consider the last number spoken, 0. Since it had been spoken before, the next number to speak is the difference between the turn number when it was last spoken (the previous turn, 4) and the turn number of the time it was most recently spoken before then (turn 1). Thus, the 5th number spoken is 4 - 1, 3.
>    Turn 6: The last number spoken, 3 had also been spoken before, most recently on turns 5 and 2. So, the 6th number spoken is 5 - 2, 3.
>   Turn 7: Since 3 was just spoken twice in a row, and the last two turns are 1 turn apart, the 7th number spoken is 1.
>    Turn 8: Since 1 is new, the 8th number spoken is 0.
>    Turn 9: 0 was last spoken on turns 8 and 4, so the 9th number spoken is the difference between them, 4.
>    Turn 10: 4 is new, so the 10th number spoken is 0.
>
>(The game ends when the Elves get sick of playing or dinner is ready, whichever comes first.)
>
>Their question for you is: what will be the 2020th number spoken? In the example above, the 2020th number spoken will be 436.
>
>Here are a few more examples:
>
>    Given the starting numbers 1,3,2, the 2020th number spoken is 1.
>    Given the starting numbers 2,1,3, the 2020th number spoken is 10.
>    Given the starting numbers 1,2,3, the 2020th number spoken is 27.
>    Given the starting numbers 2,3,1, the 2020th number spoken is 78.
>    Given the starting numbers 3,2,1, the 2020th number spoken is 438.
>    Given the starting numbers 3,1,2, the 2020th number spoken is 1836.
>
>Given your starting numbers, what will be the 2020th number spoken?
>
>--- Part Two ---
>
>Impressed, the Elves issue you a challenge: determine the 30000000th number spoken. For example, given the same starting numbers as above:
>
>    Given 0,3,6, the 30000000th number spoken is 175594.
>    Given 1,3,2, the 30000000th number spoken is 2578.
>    Given 2,1,3, the 30000000th number spoken is 3544142.
>    Given 1,2,3, the 30000000th number spoken is 261214.
>    Given 2,3,1, the 30000000th number spoken is 6895259.
>    Given 3,2,1, the 30000000th number spoken is 18.
>    Given 3,1,2, the 30000000th number spoken is 362.
>
> Given your starting numbers, what will be the 30000000th number spoken?


[reddit solution with numba by Chitinid](https://www.reddit.com/r/adventofcode/comments/kdf85p/2020_day_15_solutions/gfzrp7j/?context=3)


My solution
![scalene my solution](images/scalene-built-in.png)

Using *numpy* and *numba* (solution found on reddit done by Chitinid)
![scalene solution with numpy](images/scalene-njit-1.png)

Using numpy with *numpy* and *numba typed* (solution found on reddit done by Chitinid)
![scalene solution with numba typed](images/scalene-njit-typed.png)
